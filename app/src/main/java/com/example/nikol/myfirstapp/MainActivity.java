package com.example.nikol.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_NAME = "com.example.nikol.myfirstapp.MESSAGE";
    public static final String EXTRA_ADDRESS = "com.example.nikol.myfirstapp.ADDRESS";
    public static final String EXTRA_DATEOFBIRTH = "com.example.nikol.myfirstapp.DATEOFBIRTH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
    /** Called when the user taps the Send button */
    public void sendData(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);

        EditText name = (EditText) findViewById(R.id.name);
        EditText address = (EditText) findViewById(R.id.address);
        DatePicker datePicker = (DatePicker) findViewById(R.id.dateofbirth);

        String nameData = name.getText().toString();

        String addressData = address.getText().toString();

        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String dateofbirthData = date.format(calendar.getTime());

        intent.putExtra(EXTRA_NAME, nameData);
        intent.putExtra(EXTRA_ADDRESS, addressData);
        intent.putExtra(EXTRA_DATEOFBIRTH, dateofbirthData);
        startActivity(intent);
    }
}
