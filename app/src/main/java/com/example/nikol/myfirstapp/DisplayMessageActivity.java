package com.example.nikol.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_NAME);
        String address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS);
        String dateofbirth = intent.getStringExtra(MainActivity.EXTRA_DATEOFBIRTH);

        // Capture the layout's TextView and set the string as its text
        TextView nameText = findViewById(R.id.nameText);
        nameText.setText(name);
        TextView addressText = findViewById(R.id.addressText);
        addressText.setText(address);
        TextView dateofbirthText = findViewById(R.id.dateofbirthText);
        dateofbirthText.setText(dateofbirth);
    }
    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
    public void goBack(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
